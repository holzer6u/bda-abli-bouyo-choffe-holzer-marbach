<?php

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class game extends Model
{
    protected $table = 'game';
    protected $primaryKey = "id";
    public $timestamps = false;


    public function character_in() {
        return $this->belongsToMany( '\gamepedia\models\character',
            'game2character',
            'game_id', 'character_id') ;
    }

    public function character_first_app() {
        return $this->hasMany( '\gamepedia\models\character','first_appeared_in_game_id');
    }

    public function developpers() {
        return $this->belongsToMany( '\gamepedia\models\company',
            'game_developers',
            'game_id', 'comp_id') ;
    }

    public function publishers() {
        return $this->belongsToMany( '\gamepedia\models\company',
            'game_publishers',
            'game_id', 'comp_id') ;
    }

    public function rated() {
        return $this->belongsToMany( '\gamepedia\models\gameRating',
            'game2rating',
            'game_id', 'rating_id') ;
    }

    function genreLie(){
        return $this->belongsToMany('\gamepedia\models\genre', 'game2genre', 'game_id','genre_id');
    }

    function est_commente(){
        return $this->hasMany('\gamepedia\models\commentaire','id');
    }

    function plateforme(){
        return $this->belongsToMany('\gamepedia\models\platform','game2platform','game_id','platform_id');
    }
/*
    public function character_first_app() {
        return $this->hasMany( '\gamepedia\models\character','first_appeared_in_game_id');
    }
*/
}
