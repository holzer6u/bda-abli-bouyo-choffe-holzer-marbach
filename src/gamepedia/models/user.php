<?php

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    protected $table = 'user';
    protected $primaryKey = "email";
    public $timestamps = false;

    public function commentaires() {
        return $this->hasMany( '\gamepedia\models\commentaire','email');
    }
}
