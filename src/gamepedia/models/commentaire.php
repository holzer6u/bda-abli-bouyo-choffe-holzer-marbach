<?php

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class commentaire extends Model
{
    protected $table = 'commentaire';
    protected $primaryKey = "id";
    public $timestamps = true;

    public function ecrit_par(){
        return $this->belongsTo('\gamepedia\models\user','email');
    }

    public function comment_jeu_associe(){
        return $this->belongsTo('\gamepedia\models\game','game_id');
    }
}