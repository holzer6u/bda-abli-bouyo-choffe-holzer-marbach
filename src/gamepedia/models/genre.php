<?php
/**
 * Created by PhpStorm.
 * User: tibo
 * Date: 09/03/2017
 * Time: 08:29
 */

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    protected $table = 'genre';
    protected $primaryKey = "id";
    public $timestamps = false;

    function lieAuJeu(){
        return $this->belongsToMany('\gamepedia\models\game', 'game2genre', 'genre_id','game_id');
    }
}