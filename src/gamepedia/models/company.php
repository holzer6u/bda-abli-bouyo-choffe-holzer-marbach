<?php

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;



class Company extends Model{
    protected $table='company';
    protected $primaryKey='id';
    public $timestamps=false;

    public function developpedGames() {
        return $this->belongsToMany( '\gamepedia\models\game',
            'game_developers',
            'comp_id', 'game_id') ;
    }

    public function publishedGames() {
        return $this->belongsToMany( '\gamepedia\models\game',
            'game_publishers',
            'comp_id', 'game_id') ;
    }

}