<?php
/**
 * Created by PhpStorm.
 * User: tibo
 * Date: 09/03/2017
 * Time: 09:31
 */

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class ratingBoard extends Model
{
    protected $table = 'rating_board';
    protected $primaryKey = "id";
    public $timestamps = false;

    public function aNote(){
        return $this->hasMany('\gamepedia\models\gameRating','rating_board_id');
    }
}