<?php
/**
 * Created by PhpStorm.
 * User: tibo
 * Date: 09/03/2017
 * Time: 09:21
 */

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class gameRating extends Model
{
    protected $table = 'game_rating';
    protected $primaryKey = "id";
    public $timestamps = false;


    public function rating() {
        return $this->belongsToMany( '\gamepedia\models\game',
            'game2rating',
            'rating_id', 'game_id') ;
    }

    public function notePar(){
        return $this->belongsTo('\gamepedia\models\ratingBoard','rating_board_id');
    }
}