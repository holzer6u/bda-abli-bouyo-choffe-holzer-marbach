<?php
/**
 * Created by PhpStorm.
 * User: tibo
 * Date: 09/03/2017
 * Time: 08:29
 */

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class character extends Model
{
    protected $table = 'character';
    protected $primaryKey = "id";
    public $timestamps = false;

    public function appears_in(){
        return $this->belongToMany('\gamepedia\models\game', 'game2character', 'character_id','game_id');
    }
}
