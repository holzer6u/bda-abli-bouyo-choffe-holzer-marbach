<?php
/**
 * Created by PhpStorm.
 * User: tibo
 * Date: 02/03/2017
 * Time: 11:41
 */

namespace gamepedia\models;
use Illuminate\Database\Eloquent\Model;

class Platform extends Model{
    protected $table='platform';
    protected $primaryKey='id';
    public $timestamps=false;

    function jeu_lie(){
        return $this->belongsToMany('\gamepedia\models\game','game2platform','platform_id','game_id');
    }
}