<?php
namespace Seance5_6\code;
require_once '../../vendor/autoload.php';
use gamepedia\models\character;
use gamepedia\models\game;
use conf\Eloquent;
use gamepedia\models\commentaire;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Slim;

Eloquent::init('../../src/conf/conf.ini');

$app = new \Slim\Slim();

$app->get("/api/games/:id", function($id){
    $slim = \Slim\Slim::getInstance();
    try {
        $game = game::select("id", "name", "alias", "deck", "description", "original_release_date")->where("id", "=", $id)->firstOrFail();
        $game_array = $game->toArray();
        $plateformes = $game->plateforme()->select("id", "name", "alias", "abbreviation")->get();

        $plateforme_array = [];
        foreach ($plateformes as $ite) {
            $plateforme_array[] = $ite_array = $ite->toArray();

        }
        $game_array['plateformes'] = $plateforme_array;

        $links = ['comments' => ['href' => "/api/games/" . $id . "/comments"],
                'characters'=>['href' => "/api/games/".$id."/characters" ] ];

        $res = json_encode(['game' => $game_array,
            'links' => $links]);
        if ($res != null) {
            $slim->response->headers->set("Content-Type", "application/json");
            $slim->response->setStatus(200);
            $slim->response->setBody($res);
        }
        }catch(ModelNotFoundException $e){
            $slim->response->headers->set("Content-Type", "application/json");
            $slim->response->setStatus(404);
            $slim->response->setBody(json_encore("pas d'id"));
        }
});

$app->get("/api/games", function () {
    $slim = \Slim\Slim::getInstance();
    $p = $slim->request->get("page");
    $prev = 0;
    $next = 0;
    $g = [];
    if (isset($p)) {
        $games = game::take(200)->offset(200 * $p)->select("id", "name", "alias", "deck")->get();
        foreach ($games as $game) {
            $url = $slim->urlFor("games");
            $g[] = ["game" => $game, "links" => ["self" => ["href" => $url . "/" . $game->id]]];
        }
        if ($p > 0)
            $prev = $p - 1;
        $next = $p + 1;
    } else {
        $games = game::take(200)->select("id", "name", "alias", "deck")->get();
        foreach ($games as $game) {
            $url = $slim->urlFor("games");
            $g[] = ["game" => $game, "links" => ["self" => ["href" => $url . "/" . $game->id]]];
        }
        $prev = 0;
        $next = 1;
    }

    $tab["games"] = $g;

    $tab["links"] = ["prev" => ["href" => $slim->urlFor("games")."?page=".$prev] , "next" => ["href" => $slim->urlFor("games")."?page=".$next]];

    $res = json_encode($tab);
    if ($res != null) {
        $slim->response->headers->set("Content-Type", "application/json");
        $slim->response->setStatus(200);
        $slim->response->setBody($res);
    }
})->name("games");


$app->get("/api/games/:id/comments", function($id){
    $slim = \Slim\Slim::getInstance();
    $coms = commentaire::select("titre","contenu","email","created_at","id")->where("game_id","=",$id)->get();
    $r = [];
    if(isset($coms)){
        foreach ($coms as $c){
            $createur = $c->ecrit_par()->select("nom","prenom")->first();
            $r[] = ["titre"=>$c->titre,"texte"=>$c->contenu,"created_at"=>$c->created_at,"id"=>$c->id,"createur"=> ["nom" => $createur->nom, "prenom" => $createur->prenom]];
        }

        $res = json_encode($r);
        $slim->response->headers->set("Content-Type","application/json");
        $slim->response->setStatus(200);
        $slim->response->setBody($res);
    }else{
        $slim->response->headers->set("Content-Type","application/json");
        $slim->response->setStatus(404);
        $slim->response->setBody("");
    }
})->name("commentaires");


$app->get("/api/games/:id/characters", function ($id){
    $slim = \Slim\Slim::getInstance();
    $res = [];
    $game = game::where("id","=",$id)->first();
    $characs = $game->character_in()->get();
    foreach($characs as $c){
        $url = "/api/characters/".$c->id;
        $res["characters"][] = ["character" => ["id" => $c->id, "name" => $c->name], "links" => ["self" => ["href" => $url]]];
    }
    $res = json_encode($res);
    $slim->response->headers->set("Content-Type","application/json");
    $slim->response->setStatus(200);
    $slim->response->setBody($res);
})->name("gameCharacters");

$app->run();