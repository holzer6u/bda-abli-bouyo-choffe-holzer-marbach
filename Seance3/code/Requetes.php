<?php
namespace Seance3\code;
require_once '../../vendor/autoload.php';
use conf\Eloquent;
use gamepedia\models\game;
use gamepedia\models\company;
use Illuminate\Database\Capsule\Manager as DB;

Eloquent::init('../../src/conf/conf.ini');

DB::connection()->enableQueryLog();

    function recupererLog(){
        $queries = DB::getQueryLog();
        //var_dump($queries);
        $n = 0;
        foreach ($queries as $q){
            $n++;
            $quer = $q["query"];
            echo "Requete $n: $quer";
            echo "\n";
            $t = $q["time"];
            echo "time : $t";
            echo "\nBindings : \n";
            $bindings = $q["bindings"];
            foreach ($bindings as $b){
                echo "  $b \n";
            }
            echo "\n";
        }
        exit;
    }

    function listeJeuxPage(){
        //$i = Game::get()->count();
        $time_start = microtime(true);
        for($j=0; $j<=1000; $j+=500){
            $res =  Game::take(500)->offset($j)->get(['name', 'deck']);
            //echo $res;
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;

    }

    function jeuxContientMario(){
        $time_start = microtime(true);
        $res = game::select("name")->where("name", 'LIKE', "% Mario %")->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;
    }

    function persoJeuxMario(){
        $time_start = microtime(true);
        $games = game::select()->where('name','like','Mario%')->get();
        foreach ($games as $g){
            $characs = $g->character_in()->select('name')->get();
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;
    }

    function marioRating(){
        $time_start = microtime(true);
        $games = game::select('id', 'name')->where('name', 'LIKE', 'Mario%')->get();
        foreach ($games as $g) {
            $rateds = $g->rated()->where('name','LIKE','%3+%')->get();
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;
    }

    /*
    listeJeuxPage();
    echo "\n";
    jeuxContientMario();
    echo "\n";
    persoJeuxMario();
    echo "\n";
    marioRating();
    */

    function listerJeuxCommencePar(){
        $time_start = microtime(true);
        $games = game::where('name', 'LIKE', 'Mario%')->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;
        echo "\n";

        $time_start = microtime(true);
        $games = game::where('name', 'LIKE', 'Sonic%')->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;
        echo "\n";

        $time_start = microtime(true);
        $games = game::where('name', 'LIKE', 'Dab%')->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;
    }

    //listerJeuxCommencePar();

/* La différence est réel. Les requetes s'executaient en 6.11s , 5.87s et 4.82s,
    après création d'index : 1.66s, 1.14s et 0.012s . */


    //jeuxContientMario();

/* Après création d'index la requete est réalise très rapidement : 0.09s. Mais n'est pas plus rapide qu'avant*/

    function compagniePays(){
        $time_start = microtime(true);
        $res = company::select("name")->where("location_country","=","France")->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time;
    }

    //compagniePays();

/* Avant création d'index : 0.218s
   Après création d'index : 0.081s
   Pas de grosse différence.*/




    function persoDansJeux(){
        $characs = game::find(12342)->character_in()->select('name','deck')->get();
    }

    function jeuxDeSony(){
        $companies = company::select()->where('name','like','%Sony%')->get();
        foreach ($companies as $c){
            $games = $c->developpedGames()->select('name')->get();
        }
    }

    function jeuxContientMario2(){
        $res = game::select("name")->where("name", 'LIKE', "% Mario %")->get();
    }

    function persoJeuxMario2(){
        $games = game::select()->where('name','like','Mario%')->get();
        foreach ($games as $g){
            $characs = $g->character_in()->select('name')->get();
        }
    }

    function premiereApparition(){
        $pers = game::select('id')->where('name', 'LIKE', '%Mario%')->get();
        foreach($pers as $p){
            $res = $p->character_first_app;
            foreach($res as $r){
                echo $r['name'];
                echo "\n";
            }
        }
    }

/*
    jeuxContientMario2(); //nb requetes : 1
    persoDansJeux(); //nb requetes : 2
    persoJeuxMario2(); //nb requetes : 91 */
    jeuxDeSony(); //nb requetes : 14
    //premiereApparition(); //nb requetes : 159
    recupererLog();


    function persoJeuxMarioAvecWith(){
        $games = game::select()->where('name','like','Mario%')->get();
        foreach ($games as $g) {
            $characs = $g->with('character_in')->get();
        }
    }
/*
    persoJeuxMarioAvecWith();
    recupererLog();
*/