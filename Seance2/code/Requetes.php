<?php

namespace Seance2\code;
require_once '../../vendor/autoload.php';
use conf\Eloquent;
use gamepedia\models\character;
use gamepedia\models\game;
use gamepedia\models\company;
use gamepedia\models\platform;
use gamepedia\models\genre;
use gamepedia\models\game_rating;
use Illuminate\Support\Facades\DB;

Eloquent::init('../../src/conf/conf.ini');

    function persoDansJeux(){
        $characs = game::find(12342)->character_in()->select('name','deck')->get();
        foreach ($characs as $c){
            echo $c->name;
            echo "\n";
            echo $c->deck;
            echo "\n \n";
        };
    }

    function persoJeuxMario(){
        $games = game::select()->where('name','like','Mario%')->get();
        foreach ($games as $g){
            $characs = $g->character_in()->select('name')->get();
            echo $g->name;
            echo ": \n";
            foreach ($characs as $c){
                echo $c->name;
                echo "\n";
            }
            echo "\n \n";
        }
    }

    function jeuxDeSony(){
        $companies = company::select()->where('name','like','%Sony%')->get();
        foreach ($companies as $c){
            $games = $c->developpedGames()->select('name')->get();
            echo $c->name;
            echo": \n";
            foreach ($games as $g){
                echo $g->name;
                echo "\n";
            }
            echo "\n \n";
        }
    }

    function rangMario(){
        $games = game::select()->where('name','like','%Mario%')->get();
        foreach ($games as $g){
            echo $g->name;
            echo ": ";
            $rateds = $g->rated()->select('name')->get();
            foreach ($rateds as $r){
                echo $r->name;
            }
            echo "\n";
        }
    }

    function mario3Perso()
    {
        $games = game::select('id','name')->where('name', 'LIKE', 'Mario%')->get();
        foreach ($games as $g) {
            $characs = $g->character_in()->select('name')->get();
            if ($characs->count() > 3) {
                echo $g->name;
                echo "\n";
            }
        }
    }

    function marioRating()
    {
        $games = game::select('id', 'name')->where('name', 'LIKE', 'Mario%')->get();
        foreach ($games as $g) {
            $rateds = $g->rated()->where('name','LIKE','%3+%')->get();
            if(sizeof($rateds)!=0){
                echo $g['name'];
                echo "\n";
            }
        }
    }

    function mario3PlusInc(){
        $games = game::where('name', 'LIKE', '%Mario%')->get();
        foreach ($games as $g){
            $gamesRate = $g->rated()->where('name','LIKE','%3+%')->get();
            if(sizeof($gamesRate) > 0){
                $gamesIncRate = $g->publishers()->where('name','LIKE','%Inc%')->get();
                if(sizeof($gamesIncRate) > 0){
                    echo $g['name'];
                    echo "\n";
                }
            }
        }
    }

    function mario3PlusIncCERO(){
        $games = game::where('name', 'LIKE', '%Mario%')->get();
        foreach ($games as $g){
            $gamesRate = $g->rated()->where('name','LIKE','%3+%')->get();
            if(sizeof($gamesRate) > 0){
                $gamesRateCero = $g->rated()->where('name','LIKE','%CERO%')->get();
               if(sizeof($gamesRateCero) > 0){
                   $gamesIncRate = $g->publishers()->where('name','LIKE','%Inc%')->get();
                   if(sizeof($gamesIncRate) > 0){
                       echo $g['name'];
                       echo "\n";
                   }
               }

            }
        }
    }

    //persoDansJeux();
    //persoJeuxMario();
    //jeuxDeSony();
    //rangMario();
    //marioRating();
    //mario3Perso();
    //mario3Perso();
   // mario3PlusInc();

    //mario3PlusIncCERO();

/*
    $gen = new Genre;
    $gen->name = 'rigodrol';
    $gen->deck = 'ceci est une deck';
    $gen->description = 'ca cest la description';
    $gen->save();

    $gen->lieAuJeu()->attach(12);
    $gen->lieAuJeu()->attach(56);
    $gen->lieAuJeu()->attach(345);
*/