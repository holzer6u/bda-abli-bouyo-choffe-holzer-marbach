<?php

namespace Seance4\code;
require_once '../../vendor/autoload.php';
use conf\Eloquent;
use gamepedia\models\commentaire;
use gamepedia\models\game;
use gamepedia\models\user;
use gamepedia\models\company;
use Illuminate\Database\Capsule\Manager as DB;

Eloquent::init('../../src/conf/conf.ini');

function partie1(){

    $format="Y-m-d";

    $user1 = new user;
    $user1->email = "email1@email.com";
    $user1->nom = "Boquet";
    $user1->prenom = "Bill";
    $user1->adresse = "12 rue Rue";
    $user1->tel = "0606060607";
    $user1->dateNaiss = \DateTime::createFromFormat($format,"1997-03-30");
    $user1->save();

    $user2 = new user;
    $user2->email = "email2@email.com";
    $user2->nom = "Croche";
    $user2->prenom = "Sarah";
    $user2->adresse = "15 rue Random";
    $user2->tel = "0606060687";
    $user2->dateNaiss = \DateTime::createFromFormat($format,"1999-02-18");
    $user2->save();

    $comm1 = new commentaire;
    $comm1->titre = "Titre 1";
    $comm1->contenu = "lorem ipsum : avion en latin";
    $comm1->email = "email1@email.com";
    $comm1->game_id = "12342";
    $comm1->save();

    $comm2 = new commentaire;
    $comm2->titre = "Titre 2";
    $comm2->contenu = "lorem ipsum abracadabra";
    $comm2->email = "email1@email.com";
    $comm2->game_id = "12342";
    $comm2->save();

    $comm3 = new commentaire;
    $comm3->titre = "Titre 3";
    $comm3->contenu = "lorem ipsum, à tes souhaits !";
    $comm3->email = "email1@email.com";
    $comm3->game_id = "12342";
    $comm3->save();

    $comm4 = new commentaire;
    $comm4->titre = "Titre 4";
    $comm4->contenu = "Trau bi1";
    $comm4->email = "email2@email.com";
    $comm4->game_id = "12342";
    $comm4->save();

    $comm5 = new commentaire;
    $comm5->titre = "Titre 5";
    $comm5->contenu = "Jad0r€ TrO0w";
    $comm5->email = "email2@email.com";
    $comm5->game_id = "12342";
    $comm5->save();

    $comm6 = new commentaire;
    $comm6->titre = "Titre 6";
    $comm6->contenu = "Wallah je kiff";
    $comm6->email = "email2@email.com";
    $comm6->game_id = "12342";
    $comm6->save();

}

function partie2User(){
    $faker = \Faker\Factory::create();
    for($i = 1; $i <= 25000; $i++){
        $user = new user;
        $user->email = $faker->unique()->email;
        $user->nom = $faker->lastname;
        $user->prenom = $faker->firstName($gender = null|'male'|'female');
        $user->adresse = $faker->address;
        $user->tel = $faker->e164PhoneNumber;
        $user->dateNaiss = $faker->date($format = 'Y-m-d', $max = 'now');
        $user->save();
    }
}

function partie2Comm(){
    $faker = \Faker\Factory::create();
    for($i = 1; $i<= 250000; $i++){
        $rend = user::all()->random(1);
        $rendEmail = $rend->email;

        $comm = new commentaire;
        $comm->titre = $faker->sentence($nbWords = 6, $variableNbWords = true);
        $comm->contenu = $faker->realText($maxNbChars = 200, $indexSize = 2);
        $comm->email = $rendEmail;
        $comm->game_id = $faker->numberBetween($min = 1, $max = 47948);
        $comm->save();
    }
}

partie1();
partie2User();
partie2Comm();
